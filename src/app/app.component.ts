import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServiceTService } from "./service/service-t.service";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  constructor(public http : HttpClient,private service:ServiceTService ){}

  title = 'dashboard';
  postiForm!: FormGroup;
  pop: boolean = false;

  public url : any 

 
  ngOnInit(): void {
    this.postiForm = this.createFormGroup();
    this.postipost();
    this.url=localStorage.getItem("url")
    console.log("my url from localstorage", this.url);
  }
  createFormGroup(): FormGroup {
    return new FormGroup({
      username: new FormControl("", [Validators.required, Validators.minLength(2)]),
      description: new FormControl("", [Validators.required, Validators.minLength(4), Validators.maxLength(80)]),
      img_url: new FormControl(localStorage.getItem("url")),

    });
  }

  postipost() {
  
    console.log(this.postiForm)
    if (this.postiForm.valid) {
      console.log(this.postiForm.value)
      this.service.postPosts(this.postiForm.value)
        .subscribe(
          data => {
            console.log(data);
            this.pop=true  
            window.location.reload()
          
          },
          error => {
            console.log( error )
             
              console.log("hh")
            

          })}
          
}
image : any ; 
onSubmit() 
{
const formData = new FormData();
formData.append('image', this.image);
this.http.post<any>(`http://localhost:3000/post/upload`, formData).subscribe(
  (res) =>{console.log(res)
    this.url=res.url   
console.log("mmmmmmmm",res.url )
   localStorage.setItem('url' , this.url)
  },
  (err) => console.log(err)  
);}
selectImage(event:any){
  if(event.target.files.length > 0 ){
    const file = event.target.files[0];
    this.image =file ; 

    console.log(this.image, "gggggg")
    this.onSubmit()

  }
}
}
